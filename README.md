# Mode Bridges

This repository contains infra-as-code and config-as-code for building, configuring, and deploying cross-mode bridges on the MemHamWAN network. This goes along with other components like `ioc-terraform` and `m17-deploy` to do things like bridging our AllStar hub to Discord and more.
