# dmr-bridge-discord

This directory builds a dockerfile image for [dmr-bridge-discord](https://github.com/jess-sys/dmr-bridge-discord).

## Local execution

As this is a docker image, we assume you already have docker running on your environment. First, build the image via:

```bash
docker build . --tag dmr-bridge-discord
```

This also builds the application within the image.

Then at this point, you can run the image:

```bash
docker run -e "TARGET_RX_ADDR=127.0.0.1:34001" -e LOCAL_RX_ADDR="0.0.0.0:32001" -e 'BOT_TOKEN=INSERT_YOUR_TOKEN_HERE' -e 'BOT_PREFIX="\!"' dmr-bridge-discord
```
