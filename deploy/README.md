# Deployment

This deploy uses helmfile + gitlab CI.

Before this was run, the mode-bridges namespace had to be added. Do that via `kubectl create namespace mode-bridges`.

## Running the deploy locally

helm plugin install https://github.com/databus23/helm-diff
brew install helmfile

Make sure you already can run kubectl commands against your env

https://managedkube.com/prometheus/operator/servicemonitor/troubleshooting/2019/11/07/prometheus-operator-servicemonitor-troubleshooting.html
